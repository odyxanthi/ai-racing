import math
import sys
import time
from panda3d.core import LPoint2d
import unittest

g_defaultTolerance = 0.000001


def pointToPointDistance(pt1, pt2):
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    return math.sqrt(dx*dx + dy*dy)


def isOnTheLeft(lineStart, lineEnd, pt):
    d = (lineEnd[0] - lineStart[0])*(pt[1] - lineStart[1]) - (lineEnd[1] - lineStart[1])*(pt[0] - lineStart[0])
    return (d > 0)


def isPointInPolygon(refPoint, polygon2D):
    windingNumber = 0

    for index in range(0, len(polygon2D)-1):
        startVertex = polygon2D[index]
        endVertex = polygon2D[index+1]

        if startVertex[1] < refPoint[1] and endVertex[1] > refPoint[1]:   # if current edge crosses the ray upwards.
            if isOnTheLeft(startVertex, endVertex, refPoint):
                windingNumber += 1
        elif startVertex[1] > refPoint[1] and endVertex[1] < refPoint[1]:  # if current edge crosses the ray downwards.
            if isOnTheLeft(startVertex, endVertex, refPoint) is False:
                windingNumber -= 1

    if windingNumber is 0:   # the point is in the polygon if the winding number != 0.
        return False
    else:
        return True


def orientation(p, q, r):
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
    if val is 0:
        return 0    # colinear
    if val > 0:
        return 1
    else:
        return 2


def segmentsIntersect(startPt1, endPt1, startPt2, endPt2):
    o1 = orientation(startPt1, endPt1, startPt2)
    o2 = orientation(startPt1, endPt1, endPt2)
    o3 = orientation(startPt2, endPt2, startPt1)
    o4 = orientation(startPt2, endPt2, endPt1)
    if (o1 is not o2) and (o3 is not o4):
        return True
    return False


def distanceToSegment(segmentStart, segmentEnd, refPoint):
    u = segmentEnd - segmentStart
    u_length = u.length()
    if u_length < g_defaultTolerance:
        return ((refPoint - segmentStart).length(), segmentStart)
    else:
        v = refPoint - segmentStart
        projLength = u.dot(v) / u_length

        if projLength < 0:
            return ((refPoint - segmentStart).length(), segmentStart)
        elif projLength > u_length:
            return ((refPoint - segmentEnd).length(), segmentEnd)
        else:
            lineNearestPt = segmentStart + u*projLength / u_length
            return ((refPoint - lineNearestPt).length(), lineNearestPt)


def distanceToPolyline(polylinePts, refPoint):
    minDist, nearestPt = float("inf"), LPoint2d(0,0)
    for i in range(len(polylinePts)-1):
        pt0 = polylinePts[i]
        pt1 = polylinePts[i+1]
        d, pt = distanceToSegment(pt0, pt1, refPoint)
        if d < minDist:
            minDist = d
            nearestPt = pt
    return minDist, nearestPt


def getMinMax(points2D):
    minX = min(points2D, key=lambda pt: pt[0])[0]
    maxX = max(points2D, key=lambda pt: pt[0])[0]
    minZ = min(points2D, key=lambda pt: pt[1])[1]
    maxZ = max(points2D, key=lambda pt: pt[1])[1]

    return LPoint2d(minX, minZ), LPoint2d(maxX, maxZ)


class TestGeoMethods(unittest.TestCase):
    def test_distanceToSegment(self):
        p1 = LPoint2d(0,0)
        p2 = LPoint2d(1,0)

        # projection falls before p1
        r1 = LPoint2d(-1, 1)
        d, pt = distanceToSegment(p1, p2, r1)
        self.assertAlmostEqual(math.sqrt(2), d, 6)
        self.assertAlmostEqual(0, (pt-p1).length(), 6)

        # projection between p1 and p2
        r1 = LPoint2d(0.5, 2)
        d, pt = distanceToSegment(p1, p2, r1)
        self.assertAlmostEqual(2, d, 6)
        self.assertAlmostEqual(0, (pt-LPoint2d(0.5,0)).length(), 6)

        # projection after p2
        r1 = LPoint2d(2, 1)
        d, pt = distanceToSegment(p1, p2, r1)
        self.assertAlmostEqual(math.sqrt(2), d, 6)
        self.assertAlmostEqual(0, (pt-p2).length(), 6)

        # segment of zero length
        r1 = LPoint2d(3, 0)
        d, pt = distanceToSegment(p1, p1, r1)
        self.assertAlmostEqual(3, d, 6)
        self.assertAlmostEqual(0, (pt-p1).length(), 6)

    def test_distanceToPolyline(self):
        pLine = [
              LPoint2d(0, 0)
            , LPoint2d(0, 1)
            , LPoint2d(1, 1)
            , LPoint2d(2, 2)
        ]

        d, pt = distanceToPolyline(pLine, LPoint2d(0.3, 0.5))
        expectedNearest, expectedDist = LPoint2d(0, 0.5), 0.3
        self.assertAlmostEqual(expectedDist, d, 6)
        self.assertAlmostEqual(0, (expectedNearest-pt).length(), 6)

        d, pt = distanceToPolyline(pLine, LPoint2d(0.3, 0.9))
        expectedNearest, expectedDist = LPoint2d(0.3, 1), 0.1
        self.assertAlmostEqual(expectedDist, d, 6)
        self.assertAlmostEqual(0, (expectedNearest-pt).length(), 6)

        d, pt = distanceToPolyline(pLine, LPoint2d(3, 3))
        expectedNearest, expectedDist = LPoint2d(2, 2), math.sqrt(2)
        self.assertAlmostEqual(expectedDist, d, 6)
        self.assertAlmostEqual(0, (expectedNearest-pt).length(), 6)

    def test_isOnTheLeft(self):
        self.assertFalse(isOnTheLeft((0,0),(10,10),(1,0)))
        self.assertTrue (isOnTheLeft((0,0),(10,10),(0,1)))

    def test_isPointInPolygon(self):
        self.assertTrue (isPointInPolygon((3,1), [(0,0), (10,0), (5,5),(0,0)]))
        self.assertFalse(isPointInPolygon((11,1), [(0,0), (10,0), (5,5),(0,0)]))
        self.assertTrue (isPointInPolygon((5,5), [(0,0), (10,0), (10,10),(0,10), (0,0)]))
        self.assertTrue (isPointInPolygon((0.1, 0.1), [(0,0), (10,0), (10,10),(0,10), (0,0)]))
        self.assertTrue (isPointInPolygon((9.9, 0.1), [(0,0), (10,0), (10,10),(0,10), (0,0)]))
        self.assertTrue (isPointInPolygon((9.9, 9.9), [(0,0), (10,0), (10,10),(0,10), (0,0)]))
        self.assertTrue (isPointInPolygon((0.1, 9.9), [(0,0), (10,0), (10,10),(0,10), (0,0)]))
        self.assertFalse(isPointInPolygon((10.1, 9.9), [(0,0), (10,0), (10,10),(0,10), (0,0)]))


if __name__ == '__main__':
    unittest.main()
