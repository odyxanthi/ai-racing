#!/usr/bin/env python

# Author: Odysseas Petrocheilos
#
# Ambitious goal of this was to try to write a reinforcement learning 
# algorithm that learns a policy for generating control signals for
# accelerating/breaking/steering so that it drives a car around a circuit
# minimizing the time spent, i.e. drives the car as fast as possible.
#
# I never found the time to work much on that, so it's just a toy where the 
# user drives the car manually.

from direct.showbase.ShowBase import ShowBase
from panda3d.core import *
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.DirectObject import DirectObject
from direct.interval.SoundInterval import SoundInterval
from direct.gui.DirectSlider import DirectSlider
from direct.gui.DirectButton import DirectButton
from direct.interval.MetaInterval import Parallel
from direct.interval.LerpInterval import LerpHprInterval
from direct.task.Task import Task
# from math import pi, sin, cos
import math
import sys
import time
import geometry


# Create an instance of ShowBase, which will open a window and set up a
# scene graph and camera.
g_accelerateKey = KeyboardButton.ascii_key('a')
g_decelerateKey = KeyboardButton.ascii_key('z')
g_turnLeftKey   = KeyboardButton.ascii_key('j')
g_turnRightKey  = KeyboardButton.ascii_key('k')

base = ShowBase()


def createPolylineNode(points):
    fmt = GeomVertexFormat.getV3()
    vdata = GeomVertexData('whatever', fmt, Geom.UHStatic)
    vdata.setNumRows(len(points))
    vertex = GeomVertexWriter(vdata, 'vertex')
    for x, z in points:
        vertex.addData3f(x, 0, z)

    prim = GeomLinestrips(Geom.UHStatic)
    for i in range(len(points)):
        prim.addVertex(i)
    prim.closePrimitive()

    geom = Geom(vdata)
    geom.addPrimitive(prim)

    node = GeomNode('gnode')
    node.addGeom(geom)
    return node


class MoveStrategy:
    def __init__(self, raceTrack):
        self._raceTrack = raceTrack

        self._maxOnTrackSpeed = 5.0
        self._maxOffTrackSpeed = 0.25

        self._onTrackAcceleration = 0.015
        self._offTrackAcceleration = 0.008

        self._onTrackDecelaration = 0.025
        self._offTrackDecelaration = 0.05

        self._onTrackTurnAngle = 0.8
        self._offTrackTurnAngle = 0.5

    def setRaceTrack(self, raceTrack):
        self._raceTrack = raceTrack

    def turn(self, car, angleInDegrees):
        return angleInDegrees

    def calcSpeed(self, car, isWithinTrack, decelerate, accelerate):
        if decelerate is True:
            accelerate = False

        speed = car.speed()
        if self._raceTrack.isWithinTrack(car.position()) is False:
            if speed > self._maxOffTrackSpeed:
                if (speed > self._maxOffTrackSpeed + self._offTrackDecelaration) or (decelerate is True):
                    speed = speed - self._offTrackDecelaration
                else:
                    speed = self._maxOffTrackSpeed
            else:
                if accelerate is True:
                    speed = min(self._maxOffTrackSpeed, speed + self._offTrackAcceleration)
                elif decelerate is True:
                    speed = max(0, speed - self._offTrackDecelaration)
        else:
            if accelerate is True:
                speed = min(self._maxOnTrackSpeed, speed + self._onTrackAcceleration)
            elif decelerate is True:
                speed = max(0, speed - self._onTrackDecelaration)
        return speed

    def calcDirection(self, car, isWithinTrack, turnLeft, turnRight):
        if isWithinTrack:
            angle = self._onTrackTurnAngle
        else:
            angle = self._offTrackTurnAngle

        if turnLeft:
            angle *= -1
        elif turnRight:
            pass
        else:
            angle *= 0

        rot = LMatrix4()
        rot.setRotateMat(angle, Vec3(0,1,0))
        forwardDir = rot.xformVec(car.forwardDir())
        return forwardDir, angle

    def calcVectorSpeed(self, car, decelerate, accelerate, turnLeft, turnRight):
        isWithinTrack = self._raceTrack.isWithinTrack(car.position())
        v = self.calcSpeed(car, isWithinTrack, decelerate, accelerate)
        d, angle = self.calcDirection(car, isWithinTrack, turnLeft, turnRight)
        return v, d, angle


class Car:
    def __init__(self, moveStrategy):
        self._moveStrategy = moveStrategy
        self.chassisNode = createPolylineNode([
              LPoint2d(0, -5)
            , LPoint2d(20, -5)
            , LPoint2d(20, 5)
            , LPoint2d(0, 5)
            , LPoint2d(0, -5)
        ])

        def translate2D(points2D, dx, dy):
            return [LPoint2d(pt[0]+dx, pt[1]+dy) for pt in points2D]

        frontWheel = [
              (0, 0), (4, 0), (4, 3), (0, 3), (0, 0)
        ]
        self.chassisNode.addChild(createPolylineNode(translate2D(frontWheel, 15, -7)))
        # np.setPos(15,0,-2)
        self.chassisNode.addChild(createPolylineNode(translate2D(frontWheel, 15, 4)))
        # np.setPos(15,0,9)

        rearWheel = [
              (0, 0), (5, 0), (5, 4), (0, 4), (0, 0)
        ]
        self.chassisNode.addChild(createPolylineNode(translate2D(rearWheel, 1, -8)))
        # np.setPos(1,0,-3)
        self.chassisNode.addChild(createPolylineNode(translate2D(rearWheel, 1, 4)))
        # np.setPos(1,0,9)

        airFilter  = [(15, -3), (19, -2),(19, 2),(15, 3),(15, -3)]
        airFilter1 = [(16, -2), (16, 2)]
        airFilter2 = [(17, -2), (17, 2)]
        self.chassisNode.addChild(createPolylineNode(airFilter))
        self.chassisNode.addChild(createPolylineNode(airFilter1))
        self.chassisNode.addChild(createPolylineNode(airFilter2))

        self.chassisNode_np = base.render.attachNewNode(self.chassisNode)
        self._speed = 0
        self._forwardDir = Vec3(1,0,0)


    def getNodePath(self):
        return self.chassisNode_np

    def position(self):
        return self.chassisNode_np.getPos()

    def getRefSegment(self):
        forward2D = (self._forwardDir[0], self._forwardDir[2])
        pos3D = self.position()
        pt01 = (pos3D[0], pos3D[2])
        pt02 = (pt01[0]+10*forward2D[0], pt01[1]+10*forward2D[1])
        return [pt01, pt02]

    def forwardDir(self):
        return self._forwardDir

    def speed(self):
        return self._speed

    def move(self, bDecelerate, bAccelerate, bTurnLeft, bTurnRight):
        self._speed, self._forwardDir, angle = self._moveStrategy.calcVectorSpeed(self, bDecelerate, bAccelerate, bTurnLeft, bTurnRight)
        # print 'move method called!!'
        pos = self.chassisNode_np.getPos()
        newPos = pos + self._forwardDir * self._speed
        self.chassisNode_np.setPos(newPos)

        prevR = self.chassisNode_np.getR()
        self.chassisNode_np.setR(prevR + angle)


class RaceTrack:
    def __init__(self, innerPolygon, outerPolygon, finishLine, sector1Line=None, sector2Line=None):
        self._innerPolygon = list(innerPolygon)
        self._outerPolygon = list(outerPolygon)
        self._finishLine = list(finishLine)
        self._sector1Line = None
        self._sector2Line = None

        if sector1Line is not None:
            self._sector1Line = list(sector1Line)
        if sector2Line is not None:
            self._sector2Line = list(sector2Line)

        polygonPts = innerPolygon + outerPolygon

        self._bboxMin, self._bboxMax = geometry.getMinMax(polygonPts)

        self._bboxRect = createPolylineNode([
              (self._bboxMin.getX(), self._bboxMin.getY())
            , (self._bboxMax.getX(), self._bboxMin.getY())
            , (self._bboxMax.getX(), self._bboxMax.getY())
            , (self._bboxMin.getX(), self._bboxMax.getY())
            , (self._bboxMin.getX(), self._bboxMin.getY())
        ])

        self._bboxRect.addChild(createPolylineNode(self._outerPolygon))
        self._bboxRect.addChild(createPolylineNode(self._innerPolygon))
        self._bboxRect.addChild(createPolylineNode(self._finishLine))
        if self._sector1Line is not None:
            self._bboxRect.addChild(createPolylineNode(self._sector1Line))
        if self._sector2Line is not None:
            self._bboxRect.addChild(createPolylineNode(self._sector2Line))

        self._raceTrack_np = base.render.attachNewNode(self._bboxRect)

    def getNodePath(self):
        return self._raceTrack_np

    def isWithinTrack_pos2d(self, pos2D):
        if geometry.isPointInPolygon(pos2D, self._outerPolygon) is False:
            return False
        else:
            if geometry.isPointInPolygon(pos2D, self._innerPolygon):
                # inside inner polygon => off track
                return False
            else:
                # inside outer polygon but outside inner polygon, => between two polygons
                return True

    def isWithinTrack(self, pos3D):
        return self.isWithinTrack_pos2d((pos3D[0], pos3D[2]))

    def isWithinTrack_pos3d(self, pos3D):
        return self.isWithinTrack_pos2d((pos3D[0], pos3D[2]))

    def getFinishLine(self):
        return self._finishLine


class LapTimer:
    def __init__(self):
        self.lapsStarted = 0
        self.lapStartTime = None
        self.lapTimes = []

    def lapStarted(self):
        now = time.time()
        self.lapsStarted += 1
        if self.lapsStarted > 1:
            lapTime = now - self.lapStartTime
            self.lapTimes.append(lapTime)
        self.lapStartTime = now


    def getLapTimes(self):
        return self.lapTimes

    def getLapsCompleted(self):
        if self.lapsStarted <= 1:
            return 0
        else:
            return self.lapsStarted - 1

    def getLastLapTime(self):
        if self.lapsStarted >= 2:   # if at least one lap completed
            return self.lapTimes[-1]
        else:
            None

    def getBestLap(self):
        if self.lapsStarted >= 2:   # if at least one lap completed
            return min(self.lapTimes)
        else:
            None

    def timeSinceLastLapStarted(self):
        if self.lapsStarted >= 1:
            return time.time() - self.lapStartTime
        else:
            return 0.000


class GameScene(DirectObject):
    def __init__(self):
        # Our standard title and instructions text
        self.title = OnscreenText(text="Panda3D: AI - racing",
                                  parent=base.a2dBottomCenter,
                                  pos=(0, 0.08), scale=0.08,
                                  fg=(1, 1, 1, 1), shadow=(0, 0, 0, .5))
        self.escapeText = OnscreenText(text="ESC: Quit", parent=base.a2dTopLeft,
                                       fg=(1, 1, 1, 1), pos=(0.06, -0.1),
                                       align=TextNode.ALeft, scale=.05)

        # Set up the key input
        self.accept('escape', sys.exit)
        self.accept('wheel_down', self.OnZoomOut)
        self.accept('wheel_up', self.OnZoomIn)

        lens = OrthographicLens()
        lens.setFilmOffset(-10, -10)
        lens.setFilmSize(200, 200)  # Or whatever is appropriate for your scene
        lens.setNearFar(-10, 10)
        self._lens = lens
        base.cam.node().setLens(lens)

        alight = AmbientLight("ambient")
        alight.setColor((0.9, 0.2, 0.9, 1))
        base.render.setLight(base.render.attachNewNode(alight))

        # Enable per-pixel lighting
        # base.render.setShaderAuto()

        # create race track
        scaling = 30
        trackInnerBound = map(lambda (x,y): (scaling*x, scaling*y), [(2, 2), (18, 2), (18, 7), (25, 7), (28, 15), (28, 16), (15, 18), (10, 18), (2, 10), (2, 2)])
        trackOuterBound = map(lambda (x,y): (scaling*x, scaling*y), [(0, 0), (20, 0), (20, 5), (27, 5), (30, 15), (30, 17), (15, 20), (10, 20), (0, 10), (0, 0)])
        finishLine = map(lambda (x,y): (scaling*x, scaling*y), [(10, -1), (10, 3)])
        self.raceTrack = RaceTrack(trackInnerBound, trackOuterBound, finishLine)

        # create Car
        self.car = Car(MoveStrategy(self.raceTrack))
        self.currentLap = 0
        self.carCrossingLine = False

        # base.camera.reparentTo(self.car.getNodePath())  --> this indeed attaches the camera to the car node but the result is not satifactory
        self.gameTask = taskMgr.add(self.gameLoop, "gameLoop")
        self.timingTask = taskMgr.add(self.timingTask, "timer")
        self.lapTimer = LapTimer()

        self.currentLapText = TextNode('current lap')
        self.currentLapText.setText("current: 0.000")
        textNodePath = aspect2d.attachNewNode(self.currentLapText)
        textNodePath.setScale(0.07)
        textNodePath.setPos(-1.0, -0.0, -0.75)

        self.lastLapText = TextNode('last lap')
        self.lastLapText.setText("last: 0.000")
        textNodePath = aspect2d.attachNewNode(self.lastLapText)
        textNodePath.setScale(0.07)
        textNodePath.setPos(-1.0, -0.0, -0.85)

        self.bestLapText = TextNode('best lap')
        self.bestLapText.setText("best: 0.000")
        textNodePath = aspect2d.attachNewNode(self.bestLapText)
        textNodePath.setScale(0.07)
        textNodePath.setPos(-1.0, -0.0, -0.95)

    def OnZoomIn(self):
        print 'onzoomin'
        (w, h) = self._lens.getFilmSize()
        sf, min_w, min_h = 0.95, 20, 20
        w = max(min_w, w * sf)
        h = max(min_h, h * sf)
        self._lens.setFilmSize(w, h)
        # self.UpdateGraphView()

    def OnZoomOut(self):
        print 'onzoomout'
        (w, h) = self._lens.getFilmSize()
        sf = 1.0 / 0.95
        self._lens.setFilmSize(w * sf, h * sf)

    def gameLoop(self, task):
        is_down = base.mouseWatcherNode.is_button_down

        accelerate = is_down(g_accelerateKey)
        decelerate = is_down(g_decelerateKey)
        turnLeft = is_down(g_turnLeftKey)
        turnRight = is_down(g_turnRightKey)

        self.car.move(decelerate, accelerate, turnLeft, turnRight)
        base.camera.setPos(self.car.getNodePath().getPos())
        return Task.cont

    def timingTask(self, task):
        refSegment = self.car.getRefSegment()
        finishLine = self.raceTrack.getFinishLine()

        crossesLine = geometry.segmentsIntersect(finishLine[0], finishLine[1], refSegment[0], refSegment[1])

        self.currentLapText.setText("current: %0.3f" % self.lapTimer.timeSinceLastLapStarted())

        if crossesLine and geometry.isOnTheLeft(finishLine[0], finishLine[1], refSegment[0]):
            if self.carCrossingLine is False:
                prevBestLap = None
                if self.lapTimer.getLapsCompleted() >=1:
                    prevBestLap = self.lapTimer.getBestLap()

                self.carCrossingLine = True
                self.currentLap += 1
                
                self.lapTimer.lapStarted()
                if self.lapTimer.getLapsCompleted() >=1:
                    print 'lap completed in ', self.lapTimer.getLastLapTime(), ' s'
                    self.lastLapText.setText("last: %0.3f" % self.lapTimer.getLastLapTime())
                    self.bestLapText.setText("best: %0.3f" % self.lapTimer.getBestLap())

                    if prevBestLap is not None:
                        diff = self.lapTimer.getLastLapTime() - prevBestLap
                        self.lastLapText.setText("last: %0.3f (%+0.3f)" % (self.lapTimer.getLastLapTime(), diff))

                print 'lap ', self.currentLap, ' starts!!!'
        else:
            if self.carCrossingLine is True:
                self.carCrossingLine = False
        return Task.cont


scene = GameScene()
base.run()
